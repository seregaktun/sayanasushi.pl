"use strict";

jQuery(document).ready(function () {
  //-----------------------------------
  // SCROLLTOP BUTTON
  //-----------------------------------
  var scrollTopIsActive = false;
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 700 && !scrollTopIsActive) {
      scrollTopIsActive = true;
      $('.scroll-top').addClass('active');
    } else if ($(this).scrollTop() < 700) {
      scrollTopIsActive = false;
      $('.scroll-top').removeClass('active');
    }
  });
  $('.scroll-top').on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    });
  }); //-----------------------------------
  // FIXED MENU
  //-----------------------------------

  var fixedMenuIsActive = false;
  var desctopMenu = $('.menu').clone().addClass('fixed');
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 250 && !fixedMenuIsActive) {
      fixedMenuIsActive = true;
      $('body').append(desctopMenu);
    } else if ($(this).scrollTop() < 250) {
      fixedMenuIsActive = false;
      desctopMenu.remove();
    }
  }); //-----------------------------------
  // SCROLL TO ANCHOR
  //-----------------------------------

  $('body').on('click', 'a.anchor-easy', function (e) {
    e.preventDefault();
    var anchor = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(anchor).offset().top - 80
    });
  }); //-----------------------------------
  // MOBILE MENU
  //-----------------------------------

  var isMenuActive = false;
  $('.mobile-menu-btn').on('click', function (e) {
    e.preventDefault();
    $('.wrapper').addClass('active-menu');
    $('.header').addClass('active-menu');
    $('.mobile-menu').addClass('active-menu');
    setTimeout(function () {
      isMenuActive = true;
    }, 10);
  });
  $('.wrapper, .mobile-menu__link').on('click', function (e) {
    if ($('.wrapper').hasClass('active-menu') && isMenuActive) {
      isMenuActive = false;
      $('.wrapper').removeClass('active-menu');
      $('.header').removeClass('active-menu');
      setTimeout(function () {
        $('.mobile-menu').removeClass('active-menu');
      }, 500);
    }
  }); //-----------------------------------
  // STOCKS
  //-----------------------------------

  $('.stock').on('click', function (e) {
    e.preventDefault();
    var title = $(this).data('title');
    var text = $(this).data('text');
    $('.stock-popup__title').text(title);
    $('.stock-popup__text').html(text);
  });
  $('.stock').magnificPopup({
    type: 'inline',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  }); //-----------------------------------------------------
  // VALIDATION
  //-----------------------------------------------------

  $('.s-order__form').validate({
    rules: {
      name: 'required',
      phone: {
        required: true,
        minlength: 10
      },
      persons: 'required',
      company: 'required',
      address: 'required',
      nip: 'required',
      street: 'required',
      house: 'required',
      entrance: 'required',
      floor: 'required',
      room: 'required',
      payment: 'required',
      checkFactura: 'required'
    },
    messages: {
      name: 'Необходимо заполнить поле',
      phone: 'Необходимо заполнить поле',
      persons: 'Необходимо заполнить поле',
      company: 'Необходимо заполнить поле',
      address: 'Необходимо заполнить поле',
      nip: 'Необходимо заполнить поле',
      street: 'Необходимо заполнить поле',
      house: 'Необходимо заполнить поле',
      entrance: 'Необходимо заполнить поле',
      floor: 'Необходимо заполнить поле',
      room: 'Необходимо заполнить поле',
      payment: 'Необходимо заполнить поле',
      checkFactura: 'Необходимо заполнить поле'
    },
    submitHandler: function submitHandler(form) {
      var data = $(form).serializeArray();
      data = data.concat(cart);
      console.log('-->', data);
    }
  }); //-----------------------------------------------------
  // FORM
  //-----------------------------------------------------

  $('.phone-mask').mask('+48 000 000 000');
  $('.s-order-form-radio-label-delivery').on('click', function (e) {
    if (e.target.nodeName !== 'INPUT') {
      var delivery = $(this).data('delivery');
      $('.s-oder-form__btn').prop('disabled', false);
      $('.s-order-delivery').hide();
      $('.s-order-delivery input').prop('hidden', true);
      $('.s-order-delivery-' + delivery).fadeIn();
      $('.s-order-delivery-' + delivery + ' input').prop('hidden', false);
      $('.s-order-form-payment').show();
    }
  });
  $('.s-order-form-radio-label-check-factura').on('click', function (e) {
    if (e.target.nodeName !== 'INPUT') {
      var check = $(this).data('check');
      $('.s-order-form__label-payment').fadeIn();

      if (check === 'check') {
        $('.s-order-check-factura-body').hide();
        $('.s-order-check-factura-body input').prop('hidden', true);
      }

      if (check === 'factura') {
        $('.s-order-check-factura-body').fadeIn();
        $('.s-order-check-factura-body input').prop('hidden', false);
      }
    }
  });
  $('.s-order-form__label-payment select').on('change', function () {
    var option = $('.s-order-form__label-payment select option:selected').val();

    if (option === 'Онлайн') {
      $('.s-order-form__error').fadeIn();
      $('.s-oder-form__btn').prop('disabled', true);
    } else {
      $('.s-order-form__error').hide();
      $('.s-oder-form__btn').prop('disabled', false);
    }
  }); //-----------------------------------------------------
  // CART
  //-----------------------------------------------------

  var cart = {
    items: [],
    summ: 0
  }; // Loading animation

  function loadingAnimation(state) {
    if (state) {
      $('.loading').css('display', 'flex');
    } else {
      $('.loading').hide();
    }
  }

  function cartNumItems() {
    var cartNumItems = 0;
    cart.items.forEach(function (item) {
      cartNumItems += item.count;
    });
    $('.header__cart-numofitems').text(cartNumItems);
    $('.header__cart-price span').text(cart.summ);
  } // draw order table


  function drawProductsTable() {
    var products = '';
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = cart.items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var item = _step.value;
        products += "\n        <tr class=\"item\">\n          <td class=\"title\">".concat(item.title, "</td>\n          <td>").concat(item.price, " z\u0142</td>\n          <td>\n            <a href=\"#\" class=\"s-order__cart-minus\" data-id=\"").concat(item.title, "\">-</a>\n            ").concat(item.count, "\n            <a href=\"#\" class=\"s-order__cart-plus\" data-id=\"").concat(item.title, "\">+</a>\n          </td>\n          <td><b>").concat(item.price * item.count, " z\u0142</b></td>\n          <td class=\"remove\">\n            <a href=\"#\" class=\"remove-btn\" data-id=\"").concat(item.title, "\">\n              <i class=\"fas fa-times\"></i>\n            </a>\n          </td>\n        </tr>\n      ");
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    $('.s-order__cart .products').html(products);
    $('.s-order__cart .summ .num').text(cart.summ);
    $('.s-order-form__title .summ').text(cart.summ);
  } // add product


  $('.js-add-product').on('click', function (e) {
    e.preventDefault();
    var itemTitle = $(this).data('title');
    var itemPrice = $(this).data('price');
    loadingAnimation(true);
    setTimeout(function () {
      loadingAnimation(false);
      $('.s-order-form-wrap').fadeIn();
      $('.s-order__subtitle-1').hide();
      $('.s-order__subtitle-2').show();
      var cartItem = cart.items.find(function (item) {
        return item.title === itemTitle;
      });

      if (cartItem) {
        cartItem.count++;
      } else {
        cart.items.push({
          title: itemTitle,
          price: itemPrice,
          count: 1
        });
      }

      cart.summ += itemPrice;
      cartNumItems();
      drawProductsTable();
    }, 1000);
  }); // minus product count

  $('body').on('click', '.s-order__cart-minus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var item = cart.items.find(function (item) {
      return item.title === id;
    });
    if (item.count === 1) return;
    item.count--;
    cart.summ -= item.price;
    cartNumItems();
    drawProductsTable();
  }); // plus product count

  $('body').on('click', '.s-order__cart-plus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var item = cart.items.find(function (item) {
      return item.title === id;
    });
    item.count++;
    cart.summ += item.price;
    cartNumItems();
    drawProductsTable();
  }); // remove product

  $('body').on('click', '.s-order__cart .remove-btn', function (e) {
    e.preventDefault();
    var result = confirm('Вы подтвержаете удаление?');

    if (result) {
      var id = $(this).data('id');

      if (cart.items.length < 2) {
        $('.s-order-form-wrap').hide();
        $('.s-order__subtitle-2').hide();
        $('.s-order__subtitle-1').fadeIn();
      }

      var item = cart.items.find(function (item) {
        return item.title === id;
      });
      var index = cart.items.findIndex(function (item) {
        return item.title === id;
      });
      cart.items.splice(index, 1);
      cart.summ -= item.price * item.count;
      cartNumItems();
      drawProductsTable();
    }
  }); //-----------------------------------------------------
  // PRELOADER
  //-----------------------------------------------------

  setTimeout(function () {
    $('.preloader').fadeOut();
  }, 1000);
});