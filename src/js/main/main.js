jQuery(document).ready(function () {

  //-----------------------------------
  // SCROLLTOP BUTTON
  //-----------------------------------
  let scrollTopIsActive = false;

  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 700 && !scrollTopIsActive) {
      scrollTopIsActive = true;
      $('.scroll-top').addClass('active');
    } else if ($(this).scrollTop() < 700) {
      scrollTopIsActive = false;
      $('.scroll-top').removeClass('active');
    }
  });

  $('.scroll-top').on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0,
    });
  });

  //-----------------------------------
  // FIXED MENU
  //-----------------------------------
  let fixedMenuIsActive = false;
  let desctopMenu = $('.menu').clone().addClass('fixed');

  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 250 && !fixedMenuIsActive) {
      fixedMenuIsActive = true;
      $('body').append(desctopMenu);
    } else if ($(this).scrollTop() < 250) {
      fixedMenuIsActive = false;
      desctopMenu.remove();
    }
  });

  //-----------------------------------
  // SCROLL TO ANCHOR
  //-----------------------------------
  $('body').on('click', 'a.anchor-easy', function (e) {
    e.preventDefault();
    let anchor = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(anchor).offset().top - 80,
    });
  });

  //-----------------------------------
  // MOBILE MENU
  //-----------------------------------
  let isMenuActive = false;

  $('.mobile-menu-btn').on('click', function (e) {
    e.preventDefault();
    $('.wrapper').addClass('active-menu');
    $('.header').addClass('active-menu');
    $('.mobile-menu').addClass('active-menu');
    setTimeout(function () {
      isMenuActive = true;
    }, 10);
  });

  $('.wrapper, .mobile-menu__link').on('click', function (e) {
    if ($('.wrapper').hasClass('active-menu') && isMenuActive) {
      isMenuActive = false;
      $('.wrapper').removeClass('active-menu');
      $('.header').removeClass('active-menu');
      setTimeout(function () {
        $('.mobile-menu').removeClass('active-menu');
      }, 500);
    }
  });

  //-----------------------------------
  // STOCKS
  //-----------------------------------
  $('.stock').on('click', function (e) {
    e.preventDefault();

    let title = $(this).data('title');
    let text = $(this).data('text');

    $('.stock-popup__title').text(title);
    $('.stock-popup__text').html(text);
  });

  $('.stock').magnificPopup({
    type: 'inline',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
  });

  //-----------------------------------------------------
  // VALIDATION
  //-----------------------------------------------------
  $('.s-order__form').validate({
    rules: {
      name: 'required',
      phone: {
        required: true,
        minlength: 10,
      },
      persons: 'required',
      company: 'required',
      address: 'required',
      nip: 'required',
      street: 'required',
      house: 'required',
      entrance: 'required',
      floor: 'required',
      room: 'required',
      payment: 'required',
      checkFactura: 'required',
    },
    messages: {
      name: 'Необходимо заполнить поле',
      phone: 'Необходимо заполнить поле',
      persons: 'Необходимо заполнить поле',
      company: 'Необходимо заполнить поле',
      address: 'Необходимо заполнить поле',
      nip: 'Необходимо заполнить поле',
      street: 'Необходимо заполнить поле',
      house: 'Необходимо заполнить поле',
      entrance: 'Необходимо заполнить поле',
      floor: 'Необходимо заполнить поле',
      room: 'Необходимо заполнить поле',
      payment: 'Необходимо заполнить поле',
      checkFactura: 'Необходимо заполнить поле',
    },
    submitHandler: function (form) {
      let data = $(form).serializeArray();
      data = data.concat(cart);
      console.log('-->', data);
    },
  });

  //-----------------------------------------------------
  // FORM
  //-----------------------------------------------------
  $('.phone-mask').mask('+48 000 000 000');

  $('.s-order-form-radio-label-delivery').on('click', function (e) {
    if (e.target.nodeName !== 'INPUT') {
      let delivery = $(this).data('delivery');
      $('.s-oder-form__btn').prop('disabled', false);
      $('.s-order-delivery').hide();
      $('.s-order-delivery input').prop('hidden', true);
      $('.s-order-delivery-' + delivery).fadeIn();
      $('.s-order-delivery-' + delivery + ' input').prop('hidden', false);
      $('.s-order-form-payment').show();
    }
  });

  $('.s-order-form-radio-label-check-factura').on('click', function (e) {
    if (e.target.nodeName !== 'INPUT') {
      let check = $(this).data('check');
      $('.s-order-form__label-payment').fadeIn();
      if (check === 'check') {
        $('.s-order-check-factura-body').hide();
        $('.s-order-check-factura-body input').prop('hidden', true);
      }
      if (check === 'factura') {
        $('.s-order-check-factura-body').fadeIn();
        $('.s-order-check-factura-body input').prop('hidden', false);
      }
    }
  });

  $('.s-order-form__label-payment select').on('change', function () {
    let option = $('.s-order-form__label-payment select option:selected').val();
    if (option === 'Онлайн') {
      $('.s-order-form__error').fadeIn();
      $('.s-oder-form__btn').prop('disabled', true);
    } else {
      $('.s-order-form__error').hide();
      $('.s-oder-form__btn').prop('disabled', false);
    }
  });

  //-----------------------------------------------------
  // CART
  //-----------------------------------------------------
  let cart = {
    items: [],
    summ: 0,
  };

  // Loading animation
  function loadingAnimation (state) {
    if (state) {
      $('.loading').css('display', 'flex');
    } else {
      $('.loading').hide();
    }
  }

  function cartNumItems () {
    let cartNumItems = 0;
    cart.items.forEach(item => {
      cartNumItems += item.count;
    });

    $('.header__cart-numofitems').text(cartNumItems);
    $('.header__cart-price span').text(cart.summ);
  }

  // draw order table
  function drawProductsTable () {
    let products = '';
    for (let item of cart.items) {
      products += `
        <tr class="item">
          <td class="title">${item.title}</td>
          <td>${item.price} zł</td>
          <td>
            <a href="#" class="s-order__cart-minus" data-id="${item.title}">-</a>
            ${item.count}
            <a href="#" class="s-order__cart-plus" data-id="${item.title}">+</a>
          </td>
          <td><b>${item.price * item.count} zł</b></td>
          <td class="remove">
            <a href="#" class="remove-btn" data-id="${item.title}">
              <i class="fas fa-times"></i>
            </a>
          </td>
        </tr>
      `;
    }

    $('.s-order__cart .products').html(products);
    $('.s-order__cart .summ .num').text(cart.summ);
    $('.s-order-form__title .summ').text(cart.summ);
  }

  // add product
  $('.js-add-product').on('click', function (e) {
    e.preventDefault();

    let itemTitle = $(this).data('title');
    let itemPrice = $(this).data('price');

    loadingAnimation(true);

    setTimeout(function () {

      loadingAnimation(false);
      $('.s-order-form-wrap').fadeIn();
      $('.s-order__subtitle-1').hide();
      $('.s-order__subtitle-2').show();

      let cartItem = cart.items.find((item) => {
        return item.title === itemTitle;
      });

      if (cartItem) {
        cartItem.count++;
      } else {
        cart.items.push({
          title: itemTitle,
          price: itemPrice,
          count: 1,
        });
      }

      cart.summ += itemPrice;

      cartNumItems();

      drawProductsTable();

    }, 1000);

  });

  // minus product count
  $('body').on('click', '.s-order__cart-minus', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    let item = cart.items.find((item) => {
      return item.title === id;
    });

    if (item.count === 1) return;

    item.count--;
    cart.summ -= item.price;

    cartNumItems();

    drawProductsTable();
  });

  // plus product count
  $('body').on('click', '.s-order__cart-plus', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    let item = cart.items.find((item) => {
      return item.title === id;
    });

    item.count++;
    cart.summ += item.price;

    cartNumItems();
    drawProductsTable();
  });

  // remove product
  $('body').on('click', '.s-order__cart .remove-btn', function (e) {
    e.preventDefault();

    let result = confirm('Вы подтвержаете удаление?');

    if (result) {
      let id = $(this).data('id');

      if (cart.items.length < 2) {
        $('.s-order-form-wrap').hide();
        $('.s-order__subtitle-2').hide();
        $('.s-order__subtitle-1').fadeIn();
      }

      let item = cart.items.find((item) => {
        return item.title === id;
      });

      let index = cart.items.findIndex((item) => {
        return item.title === id;
      });

      cart.items.splice(index, 1);
      cart.summ -= item.price * item.count;

      cartNumItems();
      drawProductsTable();
    }

  });

  //-----------------------------------------------------
  // PRELOADER
  //-----------------------------------------------------
  setTimeout(function () {
    $('.preloader').fadeOut();
  }, 1000);

});
